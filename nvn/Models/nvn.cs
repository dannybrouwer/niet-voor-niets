﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace nvn.Models
{
    public class nvn : DbContext
    {
        public DbSet<klassen> Klassen { get; set; }
        public DbSet<studenten> Studenten { get; set; }
    }
}