﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace nvn.Models
{
    public class studenten
    {
        public int studentId { get; set; }
        public string studentName { get; set; }
        public string studentEmail { get; set; }
    }
}